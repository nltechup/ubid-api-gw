import { Module, NestModule, MiddlewareConsumer, RequestMethod } from '@nestjs/common';

import { OfferModule } from './offer/offer.module';
import { SharedModule } from './shared/shared.module';
import { AuthMiddleware } from './shared/middleware/auth.middleware';
import { OfferController } from './offer/offer.controller';
import { ProxyMiddlewareFactory } from './shared/middleware/proxy.middleware.factory';
import { LoginModule } from './login/login.module';
import { LoginController } from './login/login.controller';
import { CategoryController } from './offer/category.controller';
import { BidModule } from './bid/bid.module';
import { BidController } from './bid/bid.controller';
import { CircuitBreakerService } from './services/circuit-breaker/circuit-breaker.service';
import { CircuitBreakerMiddleware } from './shared/middleware/circuitBreaker.middleware';

@Module({
  imports: [ OfferModule, SharedModule, LoginModule, BidModule ],
  providers: [ CircuitBreakerService ],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(AuthMiddleware).forRoutes({ path: '/api/offers/me', method: RequestMethod.GET });
    consumer.apply(AuthMiddleware).forRoutes({ path: '/api/offers', method: RequestMethod.POST });
    consumer.apply(AuthMiddleware).forRoutes({ path: '/api/auth/logout', method: RequestMethod.DELETE });
    consumer.apply(AuthMiddleware).forRoutes(BidController);

    // tslint:disable-next-line:max-line-length
    consumer.apply(CircuitBreakerMiddleware, ProxyMiddlewareFactory.createProxy(OfferController)).forRoutes(OfferController);
    consumer.apply(ProxyMiddlewareFactory.createProxy(LoginController)).forRoutes(LoginController);
    consumer.apply(ProxyMiddlewareFactory.createProxy(CategoryController)).forRoutes(CategoryController);
    consumer.apply(CircuitBreakerMiddleware, ProxyMiddlewareFactory.createProxy(BidController)).forRoutes(BidController);
  }
}
