import { NestFactory } from '@nestjs/core';
import { NestApplicationOptions } from '@nestjs/common/interfaces/nest-application-options.interface';

import { AppModule } from './app.module';

(async () => {
  try {
    const port: number = parseInt(process.env.NODE_PORT, 10) || 8082;

    const options: NestApplicationOptions = {
      bodyParser: true,
      cors: true,
    };

    const app = await NestFactory.create(AppModule, options);

    await app.listen(port);
  } catch (e) {
    // tslint:disable-next-line:no-console
    console.log(`Error creating api-gw: ${JSON.stringify(e)}`);
  }
})();
