import { Get, Controller, Post } from '@nestjs/common';

import { ProxyController } from '../utils';

@Controller('/api/bids')
@ProxyController(process.env.BID_SRV_PROTOCOL,
    process.env.BID_SRV_HOST,
    Number.parseInt(process.env.BID_SRV_PORT, 10))
export class BidController {
    @Get('mine')
    // tslint:disable-next-line:no-empty
    async getMyBids(): Promise<void> {}

    @Get('count')
    // tslint:disable-next-line:no-empty
    async getBidCount(): Promise<void> {}

    @Post()
    // tslint:disable-next-line:no-empty
    async createBid(): Promise<void> {}
}
