import { Module, HttpModule, Global } from '@nestjs/common';
import { AuthenticationService } from './auth/auth.service';
import { AuthMiddleware } from './middleware/auth.middleware';
import { LoggingService } from './logging/logging.service';
import { loggingServiceFactory } from './logging/logging.factory';

@Global()
@Module({
  imports: [
    HttpModule.register({
      timeout: 5000,
      maxRedirects: 5,
    }),
  ],
  providers: [
    { provide: LoggingService, useFactory: loggingServiceFactory },
    AuthenticationService,
    AuthMiddleware,
  ],
  exports: [
    LoggingService,
    AuthenticationService,
    AuthMiddleware,
  ],
})
export class SharedModule {}
