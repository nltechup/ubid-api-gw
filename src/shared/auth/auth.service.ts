import { Injectable, HttpService } from '@nestjs/common';
import { Observable } from 'rxjs';

@Injectable()
export class AuthenticationService {
    private readonly authUrl: string;

    constructor(private httpService: HttpService) {
        this.authUrl = `${process.env.AUTH_SRV_PROTOCOL}://${process.env.AUTH_SRV_HOST}:${process.env.AUTH_SRV_PORT}`;
    }

    public authenticate(token: string): Observable<any> {
        return this.httpService.post(`${this.authUrl}/auth/validatetoken`, { token });
    }
}
