import { Injectable, NestMiddleware, HttpStatus } from '@nestjs/common';
import { tap } from 'rxjs/operators';
import { Request, Response } from 'express';
import { CircuitBreakerService } from '../../services/circuit-breaker/circuit-breaker.service';
import e = require('express');

@Injectable()
export class CircuitBreakerMiddleware implements NestMiddleware {
    use(req: Request, res: Response, next: () => void) {
        const path = req.originalUrl.replace('/api', '');

        if (CircuitBreakerService.searchForOpenCircuit(path)) {
            res.send({ break: true });
        } else {
            next();
        }
    }
}
