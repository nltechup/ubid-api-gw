import { Injectable, NestMiddleware, HttpStatus } from '@nestjs/common';
import { tap } from 'rxjs/operators';
import { Request, Response } from 'express';
import { AuthenticationService } from '../auth/auth.service';

@Injectable()
export class AuthMiddleware implements NestMiddleware {
    constructor(private authService: AuthenticationService) { }
    use(req: Request, res: Response, next: () => void) {
        // check for a header named x-token
        if (req.headers['x-token']) {
            const token = req.headers['x-token'] as string;

            // validate the x-token
            this.authService.authenticate(token)
                            .pipe(
                                tap(value => {
                                    // if the token is valid, set it as a new header named 'user', it'll become the new request for the next function
                                    if (value && value.data && value.data.userId) {
                                        req.headers['x-user-uuid'] = value.data.userId;
                                        delete req.headers['x-token'];
                                    }
                                }),
                            )
                            .subscribe(() => next(), (error) => res.status(HttpStatus.UNAUTHORIZED).end());
        } else {
            res.status(HttpStatus.UNAUTHORIZED).end();
        }
    }
}
