import { Response, Request, NextFunction } from 'express';
import * as proxy from 'express-http-proxy';
import * as winston from 'winston';

import { CircuitBreakerService } from '../../services/circuit-breaker/circuit-breaker.service';

export class ProxyMiddlewareFactory {
    public static createProxy(controller: any) {
        return proxy(controller.PROXY_HOST, {
            preserveHostHdr: true,
            proxyReqPathResolver: (req: Request) => {
                const urlParts = req.url.split('?');
                const queryString = urlParts[1];
                const forwardPath = urlParts[0].replace('/api', '');

                return forwardPath + `${queryString ? `?${queryString}` : ''}`;
            },
            proxyErrorHandler: (err: any, res: Response, next: NextFunction) => {
                if (err) {
                    const path = res.req.originalUrl.replace('/api', '');

                    CircuitBreakerService.fail(path);
                }
            },
            skipToNextHandlerFilter: (res: Response) => {
                const path = res.req.path;

                CircuitBreakerService.success(path);
                return false;
            },
        });
    }
}
