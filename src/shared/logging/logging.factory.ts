import * as winston from 'winston';

import { LoggingService } from './logging.service';

export const loggingServiceFactory = async () => {
    const loggingOptions: winston.LoggerOptions = {
        level: 'info',
        format: winston.format.json(),
        transports: [
            new winston.transports.Console({
                format: winston.format.simple(),
            }),
        ],
    };

    const loggingService = new LoggingService(loggingOptions);
    const logger = await loggingService.connect();

    return loggingService;
};
