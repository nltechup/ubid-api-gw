export function ProxyController(proxyProtocol: string, proxyHost: string, proxyPort: number): ClassDecorator {
    return (target: object) => {
        Reflect.defineProperty(target, 'PROXY_HOST', { value: `${proxyProtocol}://${proxyHost}:${proxyPort}/` });
    };
}
