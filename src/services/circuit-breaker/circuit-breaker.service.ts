import { Injectable } from '@nestjs/common';
import * as moment from 'moment';

import { CircuitBreakerModel, CircuitBreakerStatus } from '../../models';

@Injectable()
export class CircuitBreakerService {
    static Breaks: CircuitBreakerModel[] = [];

    public static searchForOpenCircuit(url: string): CircuitBreakerModel {
        let result = null;

        const breakCircuit = CircuitBreakerService.findBreaker(url);

        if (breakCircuit && breakCircuit.status === CircuitBreakerStatus.Open) {
            if (CircuitBreakerService.getIsOpenUntilExpired(breakCircuit.openTill)) {
                // if the timeout period has expired, set the status to half-open, don't reset the fail count
                breakCircuit.status = CircuitBreakerStatus.HalfOpen;
                breakCircuit.openTill = null;
            } else {
                result = breakCircuit;
            }
        }

        return result;
    }

    public static fail(url: string) {
        const breakCircuit = CircuitBreakerService.findBreaker(url);

        if (breakCircuit) {
            // circuit was opened before, test for fail count
            const failCountExceeded = breakCircuit.failureCount === parseInt(process.env.CB_MAX_FAIL_COUNT, 10);

            if (failCountExceeded) {
                // open the circuit
                breakCircuit.status = CircuitBreakerStatus.Open;
                breakCircuit.openTill = CircuitBreakerService.setOpenUntilValue();
            } else {
                breakCircuit.failureCount++;
            }
        } else {
            CircuitBreakerService.Breaks.push(new CircuitBreakerModel(url));
        }
    }

    public static success(url: string) {
        const breakCircuit = CircuitBreakerService.findBreaker(url);

        if (breakCircuit) {
            // the state was half-open, now remove it
            CircuitBreakerService.removeBreaker(breakCircuit);
        }
    }

    static findBreaker(url: string): CircuitBreakerModel {
        if (CircuitBreakerService.Breaks && CircuitBreakerService.Breaks.length) {
            return CircuitBreakerService.Breaks.find(val => val.url.toLowerCase().trim() === url.toLowerCase().trim());
        }

        return null;
    }

    static removeBreaker(breaker: CircuitBreakerModel): void {
        const index = CircuitBreakerService.Breaks.indexOf(breaker);

        if (index > -1) {
            CircuitBreakerService.Breaks.splice(index, 1);
        }
    }

    static setOpenUntilValue() {
        const now = moment();
        return now.add(parseInt(process.env.CB_TIMEOUT_SEC, 10), 'second');
    }

    static getIsOpenUntilExpired(openUtil: moment.Moment): boolean {
        const now = moment();

        return now.isSameOrAfter(openUtil);
    }
}
