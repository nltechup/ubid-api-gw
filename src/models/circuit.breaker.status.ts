export enum CircuitBreakerStatus {
    Closed = 'CLOSED',
    Open = 'OPEN',
    HalfOpen= 'HALF_OPEN',
}
