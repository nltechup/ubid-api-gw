import * as moment from 'moment';

import { CircuitBreakerStatus } from './';

export class CircuitBreakerModel {
    public url: string;
    public status: CircuitBreakerStatus;
    public failureCount: number;
    public openTill: moment.Moment;

    constructor(url: string) {
        this.url = url;
        this.status = CircuitBreakerStatus.Closed;
        this.failureCount = 1;
        this.openTill = null;
    }
}
