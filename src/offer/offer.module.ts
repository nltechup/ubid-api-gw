import { Module } from '@nestjs/common';

import { OfferController } from './offer.controller';
import { CategoryController } from './category.controller';

@Module({
  controllers: [ OfferController, CategoryController ],
})
export class OfferModule {}
