import { Get, Controller } from '@nestjs/common';

import { ProxyController } from '../utils';

@Controller('api/categories')
@ProxyController(process.env.OFFER_SRV_PROTOCOL,
                process.env.OFFER_SRV_HOST,
                Number.parseInt(process.env.OFFER_SRV_PORT, 10))
export class CategoryController {
    @Get()
    // tslint:disable-next-line:no-empty
    async getCategory(): Promise<void> {}
}
