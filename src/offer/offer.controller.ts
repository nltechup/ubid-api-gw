import { Get, Controller, Post, Param } from '@nestjs/common';

import { ProxyController } from '../utils';

@Controller('api/offers')
@ProxyController(process.env.OFFER_SRV_PROTOCOL,
                process.env.OFFER_SRV_HOST,
                Number.parseInt(process.env.OFFER_SRV_PORT, 10))
export class OfferController {
    @Get('me')
    // tslint:disable-next-line:no-empty
    async getMyOffers(): Promise<void> {}

    @Get(':offerId')
    // tslint:disable-next-line:no-empty
    async getOfferById(@Param('offerId') offerId: string): Promise<void> {}

    @Get()
    // tslint:disable-next-line:no-empty
    async getOffer(): Promise<void> {}

    @Post()
    // tslint:disable-next-line:no-empty
    async createOffer(): Promise<void> {}
}
