import { Controller, Post, Body, Delete } from '@nestjs/common';

import { ProxyController } from '../utils';

@Controller('api/auth')
@ProxyController(process.env.AUTH_SRV_PROTOCOL,
    process.env.AUTH_SRV_HOST,
    Number.parseInt(process.env.AUTH_SRV_PORT, 10))
export class LoginController {
    @Post('login')
    // tslint:disable-next-line:no-empty
    async login(): Promise<void> {}

    @Post('validateToken')
    // tslint:disable-next-line:no-empty
    async validateToken(): Promise<void> {}

    @Delete('logout')
    // tslint:disable-next-line:no-empty
    async logout(): Promise<void> {}
}
