FROM node:alpine as build
WORKDIR /app/
ADD ["tsconfig.json", "package.json", "package-lock.json", "./"]
RUN npm i --silent
ADD src/ src/
RUN node_modules/.bin/tsc

FROM node:alpine
COPY --from=build /app/bin/ /app/bin/
COPY --from=build /app/node_modules/ /app/node_modules/
COPY --from=build /app/package.json /app/
EXPOSE 8082
WORKDIR /app/
ENTRYPOINT [ "npm", "start" ]